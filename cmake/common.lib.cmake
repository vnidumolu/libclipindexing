###############################################################################
# @ file CMakeLists.txt
#
# CMakeLists.txt common for libs
#
# @author David Park <dpark@alarm.com>
# @date   
#
###############################################################################
#
# 
# 
#
###############################################################################

set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fPIC" )
set (CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -fPIC" )

if(CMAKE_BUILD_TYPE STREQUAL "Debug")
    get_property( CPPUTEST_HOME GLOBAL PROPERTY CPPUTEST_HOME )
endif(CMAKE_BUILD_TYPE STREQUAL "Debug")

