###############################################################################
# @ file CMakeLists.txt
#
# CMakeLists.txt arm toolchain sav530
#
# @author David Park <dpark@alarm.com>
# @date   2019-01-04
#
###############################################################################
#
# Copyright (C) 2020 Alarm.com Inc.
#
###############################################################################

set(TOOLCHAIN_BINDIR ${CMAKE_SYSROOT}/../../bin )
set(TOOLCHAIN_PREFIX x86_64-linux-)

SET(CMAKE_SYSTEM_NAME Linux)

set(CMAKE_C_COMPILER "${TOOLCHAIN_BINDIR}/${TOOLCHAIN_PREFIX}gcc")
set(CMAKE_CXX_COMPILER "${TOOLCHAIN_BINDIR}/${TOOLCHAIN_PREFIX}g++")

set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_PACKAGE ONLY)
