###############################################################################
# @ file CMakeLists.txt
#
# CMakeLists.txt common
#
# @author David Park <dpark@alarm.com>
# @date   
#
###############################################################################
#
# 
# 
#
###############################################################################

# Show all compiler warnings.
if(CMAKE_BUILD_TOOL MATCHES "make")
  if(NOT CMAKE_CXX_FLAGS MATCHES "-Wall")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall")
  endif(NOT CMAKE_CXX_FLAGS MATCHES "-Wall")
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++0x -Wno-write-strings -Wno-unused-function")
  if(NOT CMAKE_C_FLAGS MATCHES "-Wall")
    set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -Wall")
  endif(NOT CMAKE_C_FLAGS MATCHES "-Wall")
  set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -std=gnu99")
endif(CMAKE_BUILD_TOOL MATCHES "make")

if(${TEST_DO_COVERAGE} MATCHES "yes")
  set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} ${CMAKE_CURRENT_LIST_DIR})
  include( CodeCoverage )
endif(${TEST_DO_COVERAGE} MATCHES "yes")

if(CMAKE_BUILD_TYPE STREQUAL "Debug")
    add_definitions( -DDEBUG=1 )
endif(CMAKE_BUILD_TYPE STREQUAL "Debug")

if(${ENABLE_IPV6})
    add_definitions( -DUSE_IPV6=1 )
endif(${ENABLE_IPV6})

add_definitions( -DTARGET_RT_LITTLE_ENDIAN=1 )

if(${ENABLE_PROFILING})
    add_definitions( -DTIME_PLATFORM=1 )
endif()

