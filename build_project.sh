#!/bin/bash
###############################################################################
# @file build_project.sh
#
# bash script to build with cmake and ninja
#
###############################################################################

usage() {
    cat <<EOF ;
Usage:
	build_project.sh [options] <platform> <architecture> [<sysroot>]

	<platform> is ubuntu, dummy, fusion or sav535
	<architecture> is x86, x64, sav530, sav535
	<sysroot> is the sysroot for cross compiling

	options:
	    -c Run cppcheck
	    -d Do not strip debug symbols
	    -m Use make instead of ninja
	    -s Do not output status messages
	    -t Compile with flags optimized for tracing memory
 	    -w Enable warnings
	    -v Verbose ninja build
EOF
    exit 8
}
  

# user settings
DOCUMENTATION=no
TESTS=yes

#supported make, ninja
CMAKE_BACKEND="ninja"

# Are we verbose
VERBOSE=
# Doing mtraces
MTRACE_FLAGS=
# Warnings enabled
GCC_WARN=

CONFIGDIR="/tmp/configs/"

LETTER_OPTIONS=$(getopt -n $0 -o cdmstvw -- "$@")
eval set -- $LETTER_OPTIONS 
while :
do
    case "$1" in
	-c) export CPPCHECK=1                ;shift;;
	-d) NO_STRIP="-DNO_STRIP=ON"         ;shift;;
	-m) CMAKE_BACKEND=make               ;shift;;
	-s) export MAKE_SILENT=1             ;shift;;
	-t) MTRACE_FLAGS="-DMTRACE_FLAGS=ON" ;shift;;
	-v) VERBOSE=-v                       ;shift;;
	-w) GCC_WARN="-DGCC_WARN=ON"         ;shift;;
	--) shift; break;;
        *)  echo "Unknown options $1 -- This should not happen"
            usage;;
    esac
done

PLATFORM_CONFIG=$1
PLATFORM_NAME=$2
SYSROOT=$3

#
# Validate the platform option
#
if [ "$PLATFORM_CONFIG" != "ubuntu" -a "$PLATFORM_CONFIG" != "dummy" -a "$PLATFORM_CONFIG" != "fusion" -a "$PLATFORM_CONFIG" != "sav535" ]; then
    echo "You must specify a platform configuration"
    echo "Options:"
    echo "  sample"
    echo "Ex: ./build_project.sh <architecture> <sysroot>"
    echo "Ex: ./build_project.sh <x86>"
    usage
fi


#
# Validate the architecture
#
if [ -z $PLATFORM_NAME ]; then
    echo "You must specify a platform architecture"
    echo "Options:"
    echo "  x86"
    echo "  x64"
    echo "  sav530"
    echo "  sav535"
    usage
fi

TOOLCHAIN_FILE=""

case $PLATFORM_NAME in
     x86|X86)
        ;;
     x64|X64)
	export PKG_CONFIG_PATH=$SYSROOT/usr/lib/pkgconfig/
        export PKG_CONFIG_SYSROOT_DIR=$SYSROOT
        TOOLCHAIN_FILE=./cmake/toolchain-x64.cmake
        ;;
     sav530|SAV530)
	if [ -z $3 ]; then
		echo "You must specify Buildroot SDK's SYSROOT"
		exit 
        fi	
        echo "Cross Compiling for sav530 platform"
	export PKG_CONFIG_PATH=$SYSROOT/usr/lib/pkgconfig/
        export PKG_CONFIG_SYSROOT_DIR=$SYSROOT
        TOOLCHAIN_FILE=./cmake/toolchain-sav530.cmake
	;;
     sav535|SAV535)
     	echo "specifiy toolchain file here/ now building for x64 machine"
        ;;
     *)
        echo "-- Unrecognized platform: $PLATFORM_NAME"
        exit
esac

mkdir -p $CONFIGDIR

# build
ROOT_DIR=$PWD
TARGET_DIR=target
rm -rf build $TARGET_DIR
mkdir $TARGET_DIR
mkdir $TARGET_DIR/lib
mkdir build && cd build

CMAKE_COMMON="                                         \
    $MTRACE_FLAGS                                      \
    $GCC_WARN                                          \
    -DCMAKE_TOOLCHAIN_FILE=$TOOLCHAIN_FILE             \
    -DCMAKE_CXX_COMPILER_LAUNCHER=ccache               \
    -DCMAKE_INSTALL_PREFIX=../$TARGET_DIR              \
    -DBUILD_DOC=$DOCUMENTATION                         \
    -DBUILD_TESTS=$TESTS                               \
    -DPLATFORM_CONFIG=$PLATFORM_CONFIG                 \
    $NO_STRIP                                          \
    -DCMAKE_SYSROOT=$SYSROOT                           \
    -DPLATFORM_NAME=$PLATFORM_NAME                     "

# Not common
#    	-DCMAKE_SYSROOT=$SYSROOT                           \
#	-DCONFIGDIR=$CONFIGDIR

if [ $CMAKE_BACKEND = "ninja" ]; then
    if [ "$TOOLCHAIN_FILE" != "" ]; then
	      cmake ..                   \
		-GNinja                  \
  	        $CMAKE_COMMON            
    else
	     cmake -GNinja ..              \
		$CMAKE_COMMON              \
		-DCONFIGDIR=$CONFIGDIR
    fi

    ninja -j4 $VERBOSE
    ninja install

elif [ $CMAKE_BACKEND = "make" ]; then
    if [ "$TOOLCHAIN_FILE" != "" ]; then
	      cmake .. $CMAKE_COMMON
    else
	      cmake ..                     \
		$CMAKE_COMMON              \
		 -DCONFIGDIR=$CONFIGDIR
    fi

    make -j4
    make install

else
    echo "Unsupported backend: options are make or ninja"
fi
