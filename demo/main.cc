#include <iostream>
#include "utcIndexing.hpp"
#include <unistd.h>
#include "timeStampTest.h"

int main()
{
	timeStampTest test(MAX_DATABASE_SIZE);
	std::cout << "call utcIndexing apis" << std::endl;
	std::cout << "  creating clipDataBase "; 
	// create class with max size .
	
	std::cout << " done " << std::endl; 

	std::cout << " inserting elements. started ----> "; 

	const auto p1 = std::chrono::system_clock::now();
	auto utc = std::chrono::duration_cast<std::chrono::seconds>(p1.time_since_epoch()).count();	
	std::cout << " utc time in seconds  " << utc << std::endl; 
	
	// unit 
	test.loadUnitTest();
	test.simulateInsertClip();
	
	// print state of the database.
	test.sampleFetchTest();
	test.testGetSDRClips();
	
	fprintf(stderr, "testing delete Sdr videos \n ");
	test.TestDeleteSDRVideos();
	
	return 0;
}
