#include <iostream>
#include "utcIndexing.hpp"
#include <unistd.h>

class timeStampTest {
public : 
	
	timeStampTest(int databaseSize) : mDatabase(new clipDatabase (databaseSize)) {}
	~timeStampTest()
	{
		delete mDatabase;
	}

	void loadUnitTest();
	void simulateInsertClip();
	void sampleFetchTest();
	void testGetSDRClips();
	void TestDeleteSDRVideos();
private : 
	clipDatabase *mDatabase;
};
	
void timeStampTest::loadUnitTest() 
{
	mDatabase->mIndex[1633973791]= {"clipNum_12_hours_36_mins_30.mp4"};
	mDatabase->mIndex[1633973795]= {"clipNum_12_hours_36_mins_34.mp4"};
	mDatabase->mIndex[1633973802]= {"clipNum_12_hours_36_mins_41.mp4"};
	mDatabase->mIndex[1633973810]= {"clipNum_12_hours_36_mins_50.mp4"};
	mDatabase->mIndex[1633973816]= {"clipNum_12_hours_36_mins_56.mp4"};
	mDatabase->mIndex[1633973820]= {"clipNum_12_hours_37_mins_0.mp4 "}; 
	mDatabase->mIndex[1633973826]= {"clipNum_12_hours_37_mins_6.mp4 "}; 
	mDatabase->mIndex[1633973833]= {"clipNum_12_hours_37_mins_13.mp4"};
	mDatabase->mIndex[1633973836]= {"clipNum_12_hours_37_mins_16.mp4"};
	mDatabase->mIndex[1633973846]= {"clipNum_12_hours_37_mins_26.mp4"};
	
	mDatabase->mIndex[1633976352]={"clipNum_13_hours_19_mins_12secs.mp4"};
	mDatabase->mIndex[1633976356]={"clipNum_13_hours_19_mins_16secs.mp4"};
	mDatabase->mIndex[1633976363]={"clipNum_13_hours_19_mins_23secs.mp4"};
	mDatabase->mIndex[1633976371]={"clipNum_13_hours_19_mins_31secs.mp4"};
	mDatabase->mIndex[1633976377]={"clipNum_13_hours_19_mins_37secs.mp4"};
	mDatabase->mIndex[1633976381]={"clipNum_13_hours_19_mins_41secs.mp4"};
	mDatabase->mIndex[1633976387]={"clipNum_13_hours_19_mins_47secs.mp4"};
	mDatabase->mIndex[1633976394]={"clipNum_13_hours_19_mins_54secs.mp4"};
	mDatabase->mIndex[1633976397]={"clipNum_13_hours_19_mins_57secs.mp4"};
	mDatabase->mIndex[1633976407]={"clipNum_13_hours_20_mins_7secs.mp4 "};
	
	mDatabase->mIndex[1633976726]={"clipNum_13_hours_25_mins_26secs.mp4"};
	mDatabase->mIndex[1633976730]={"clipNum_13_hours_25_mins_30secs.mp4"};
	mDatabase->mIndex[1633976737]={"clipNum_13_hours_25_mins_37secs.mp4"};
	mDatabase->mIndex[1633976745]={"clipNum_13_hours_25_mins_45secs.mp4"};
	mDatabase->mIndex[1633976751]={"clipNum_13_hours_25_mins_51secs.mp4"};
	mDatabase->mIndex[1633976755]={"clipNum_13_hours_25_mins_55secs.mp4"};
	mDatabase->mIndex[1633976761]={"clipNum_13_hours_26_mins_1secs.mp4 "};
	mDatabase->mIndex[1633976768]={"clipNum_13_hours_26_mins_8secs.mp4 "};
	mDatabase->mIndex[1633976771]={"clipNum_13_hours_26_mins_11secs.mp4"};
	mDatabase->mIndex[1633976781]={"clipNum_13_hours_26_mins_21secs.mp4"};
	
}

void timeStampTest::simulateInsertClip()
{
	long int insertCnt = 0;
	for (int i = 0; i < 10; i++) {
		
		insertCnt++;

		time_t now = time(NULL);
		struct tm  *tm = localtime(&now);
		std::string clipName = "clipNum_" + std::to_string(tm->tm_hour) + "_hours_" + std::to_string(tm->tm_min)+ "_mins_" + std::to_string(tm->tm_sec) + "secs.mp4";

		const auto p1 = std::chrono::system_clock::now();
		auto utc = std::chrono::duration_cast<std::chrono::seconds>(p1.time_since_epoch()).count();		
		
		mDatabase->insertClip(utc, clipName);
		int wait = rand() % 10 + 1;
		sleep(wait);
		std::cout << " sleep " << wait << "sec" << std::endl; 
	}
	std::cout << " <---------- inserting elements done" << std::endl; 
} 

void timeStampTest::sampleFetchTest()
{
	// this works well if loadUnitTest was called before.
	std::string fetchClipName;
	int status = mDatabase->fetchClip(1633973820, fetchClipName);
	
	if (!status) {
		std::cout << "fetched " << fetchClipName << std::endl;
	} else {
		std::cout << status <<  " error fetching" << std::endl;
	}
	
	status = mDatabase->fetchClip(1633973816, fetchClipName);
	
	if (!status) {
		std::cout << "fetched " << fetchClipName << std::endl;
	} else {
		std::cout << status <<  " error fetching 2021::7::20-> 23::52::0" << std::endl;
	}
	
	status = mDatabase->fetchClip(1633973836, fetchClipName);
	
	if (!status) {
		std::cout << "fetched " << fetchClipName << std::endl;
	} else {
		std::cout << status <<  " error fetching" << std::endl;
	}
}

void timeStampTest::testGetSDRClips()
{
	fprintf(stderr, "test 1 ");

	std::vector<std::string> clipsInRange;
	mDatabase->getSDRClips(1633973791, 1633973846, clipsInRange);
	
	fprintf(stderr, "following clips are found between %lu and %lu times \n ",(uint64_t)1633973791,(uint64_t)1633973846);
	
	for (auto c : clipsInRange) {
		fprintf(stderr,"%s \n", c.c_str());
	}
	clipsInRange.clear();

	fprintf(stderr, "test 2 ");	
	mDatabase->getSDRClips(1633973797, 1633973843, clipsInRange);
	
	fprintf(stderr, "following clips are found between %lu and %lu times \n",(uint64_t)1633973797,(uint64_t)1633973843);
	
	for (auto c : clipsInRange) {
		fprintf(stderr,"%s \n", c.c_str());
	}
	clipsInRange.clear();
}

void timeStampTest::TestDeleteSDRVideos()
{
	fprintf(stderr, "test 1 ");
	std::string b4file = "before_delete.txt";
	mDatabase->dumpDatabasetoFile(b4file);
	mDatabase->deleteSDRVideos(1633973797, 1633973843);
	std::string afterFile = "after_delete.txt";
	mDatabase->dumpDatabasetoFile(afterFile);
}
