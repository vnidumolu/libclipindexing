#include "utcIndexing.hpp"
#include <string.h>
#include <time.h>

#include <unistd.h>
#include <chrono>
#include <stdlib.h>
#include <fstream>

#undef STANDALONE_TEST_CODE
//#define STANDALONE_TEST_CODE

//========================================================================================
//              Class implementation.
//========================================================================================

clipDatabase::clipDatabase(int maxDatabaseSize):
		mCurrSize(0),
		mMaxSize(maxDatabaseSize){}

clipDatabase::~clipDatabase()
{
	if (!mIndex.empty()) {
		mIndex.clear();
	}	
}	


void clipDatabase::deleteOldestEntry()
{  
   if (mIndex.empty()) {
	   fprintf(stderr, "unable to delete anything from  an empty container");
	   return; 
   }
   std::lock_guard<std::mutex> lock_guard(mutex_);
   mIndex.erase(mIndex.begin());
   mCurrSize--;
}	

// API that backend requests clips from the camera.

void clipDatabase::insertClip(uint64_t entry, std::string &clipPath)
{
	if (getNumClips() == mMaxSize) {
		deleteOldestEntry();
	} else {
		mCurrSize++;
	}	
	std::lock_guard<std::mutex> lock_guard(mutex_);
	mIndex.insert(std::pair<uint64_t, sIndexMetadata>(entry, {clipPath}));
}

int clipDatabase::getSDRClips(uint64_t startDuration, uint64_t endDuration, std::vector<std::string> &clips)
{
	std::lock_guard<std::mutex> lock_guard(mutex_);
	auto low = startDuration == 0 ? mIndex.begin() : mIndex.lower_bound(startDuration);
	auto high = endDuration == 0 ? mIndex.end() : mIndex.upper_bound(endDuration);
	
	for (auto it = low; it != high; it++) {
		clips.push_back(it->second.mClipName);
	}
	return 0;
}

void clipDatabase::dumpDatabasetoFile(std::string &fileName)
{
	std::ofstream outfile;
	outfile.open(fileName,std::ofstream::out | std::ofstream::app);

	for (auto m : mIndex) {
		outfile << " " << m.first << " " << m.second.mClipName << std::endl; 
	}
	outfile.close();	
}

void clipDatabase::deleteSDRVideos(uint64_t startDuration, uint64_t endDuration)
{
	std::lock_guard<std::mutex> lock_guard(mutex_);
	auto low = startDuration == 0 ? mIndex.begin(): mIndex.lower_bound(startDuration);
	auto high = endDuration == 0 ? mIndex.end() : mIndex.upper_bound(endDuration);
	
	mIndex.erase(low, high);
}

// get the time GetSDRCalendar()
//  GetSDRTimeLines().

int  clipDatabase::fetchClip(uint64_t clipLocation, std::string &clipPath)
{
	std::lock_guard<std::mutex> lock_guard(mutex_);
	auto it = mIndex.find(clipLocation);
	if (it == mIndex.end()){
		fprintf(stderr,"%lu not found in the database \n", clipLocation);
		return -1;
	}
	clipPath = it->second.mClipName;
	return 0;	
}


