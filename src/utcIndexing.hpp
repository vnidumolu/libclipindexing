#ifndef __UTC_INDEXING_H_
#define __UTC_INDEXING_H_
#include <stdio.h>
#include <iostream>
#include <map>
#include <chrono>
#include <stdint.h>
#include <assert.h>
#include <stdlib.h>
#include <string>
#include <time.h>
#include <vector>
#include <mutex>

#define MAX_DATABASE_SIZE (1024)
#undef ENABLE_DEBUG_PRINTS

/* main APIs implemented so far.
-> ability to delete between startDuration and endDuration
-> fetch between startDuration and endDuration ( ability to have lower_bound and upper_bound access very quickly)
-> insert (automatically insertions will be in increasing order.)
*/
using namespace std;

// if we receive a string of date, may be converting to struct is helpful 
typedef struct {
	int year;
	int month;
	int day;
} sDate;

class clipDatabase {

public:
	typedef struct {
		std::string mClipName;
		// TODO: add startTime, endTime, Iframe list, thumbNail details etc.
	} sIndexMetadata;

	using utcClockDbMap = std::map <uint64_t, sIndexMetadata>;
	
	// create database with maximum number of entries allowed.
	clipDatabase(int maxDatabaseSize);	
	 
	 // in destructor delete all remaining entries.
	~clipDatabase();
	
	clipDatabase(clipDatabase const&) = delete;
    clipDatabase& operator=(clipDatabase other) = delete;

	// Insert clips using this API. 
	//(before inserting , 
	//call deleteExpiredClips() : If there is any retention criteria that we need to satisfy and 
	//if size on sdcard is still not  sufficient, then call
	// deleteOldestEntry()
	void insertClip(uint64_t entry, std::string &clipPath);
	
	// Backend API responses.	
	int getSDRClips(uint64_t startDuration, uint64_t endDuration, std::vector<std::string> &clips);	
	void deleteSDRVideos(uint64_t startDuration, uint64_t endDuration);
	
	// fetch single clip. (TODO: is this required )
	int fetchClip(uint64_t clipLocation, std::string &clipName);	 //TODO: implement the same.

	void dumpDatabasetoFile(std::string &fileName);

	int getNumClips() const {
		return mCurrSize;
	};

private:
	friend class timeStampTest;
	//  Delete oldest clip, even if it is within expiry date. 
	// This is to be called only when clips are not sufficient, even after deleting expired clips..
	void deleteOldestEntry();

	// use it to calculate expiry date
	sDate getTodaysDate();

	// expiry date = (todaysDate - mRetentionDays)
	sDate getExpiryDate();

	// delete clips older than getExpiryDate()
	void deleteExpiredClips();

	utcClockDbMap  mIndex;	// data base holding all the clips based on year, month, day and seconds (offset from UTC of today)
	int mCurrSize;  // count of num clips present in the database.
	int mMaxSize;   // maximum size of the database (calculated from SetSDRParameters(..,.., int ReserveSize))
	int mRetentionDays; // number of days to retain the clip (if needed, input comes from ). 
	int mbDeleted; // number of MB of data deleted. required by getSDRHistory();

	std::mutex mutex_;
};
#endif // __UTC_INDEXING_H_