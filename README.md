## Usage

### Prerequisites

You will need:

 * latest adc-buildroot sdk 
 * extract and intall in /opt folder



### Building The Project

export BUILDROOT=/opt/arm-buildroot-linux-gnueabihf_sdk-buildroot
./build_project.sh fusion sav530 $BUILDROOT/arm-buildroot-linux-gnueabihf/sysroot



#### Project Structure

Relavent top level Folder
`src` is the sources, and `demo` is where we put our unit tests.

Once the build is sucessfull, the library and executable are placed in below path.
target/lib
target/bin
